#include "GenerateurImageCarte.h"


GenerateurImageCarte::GenerateurImageCarte()
{}

GenerateurImageCarte::~GenerateurImageCarte()
{
}



void GenerateurImageCarte::push_black(ofstream& img) {
	img << 0 << " " << 0 << " " << 0 << endl;
}
void GenerateurImageCarte::push_white(ofstream& img) {
	img << 255 << " " << 255 << " " << 255 << endl;
}
void GenerateurImageCarte::push_blue(ofstream& img) {
	img << 0 << " " << 0 << " " << 255 << endl;
}
void GenerateurImageCarte::push_red(ofstream& img) {
	img << 255 << " " << 0 << " " << 0 << endl;
}
void GenerateurImageCarte::push_green(ofstream& img) {
	img << 0 << " " << 255 << " " << 0 << endl;
}
void GenerateurImageCarte::push_purple(ofstream& img) {
	img << 0 << " " << 255 << " " << 255 << endl;
}
void GenerateurImageCarte::push_pink(ofstream& img) {
	img << 255 << " " << 170 << " " << 200 << endl;
}
void GenerateurImageCarte::push_yellow(ofstream& img) {
	img << 255 << " " << 255 << " " << 0 << endl;
}
void GenerateurImageCarte::push_lightBlue(ofstream& img) {
	img << 255 << " " << 0 << " " << 255 << endl;
}
void GenerateurImageCarte::push_gray(ofstream& img) {
	img << 125 << " " << 125 << " " << 125 << endl;
}
void GenerateurImageCarte::GenerateGrid(ofstream& img, int height, int width, vector<vector<uint8_t> > &matrix) {
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			uint8_t type = matrix[x][y];
			switch (type)
			{
			case 0:push_white(img);
				break;
			case 1:	push_black(img);
				break;
			case 2: push_gray(img);
				break;
			case 3: push_blue(img);
				break;
			case 4: push_green(img);
				break;
			case 5: push_red(img);
				break;
			case 6: push_pink(img);
				break;
			case 7: push_yellow(img);
				break;
			case 8: push_purple(img);
				break;
			case 9: push_lightBlue(img);
				break;
			default: push_white(img);
				break;

			}
		}
	}
}

void GenerateurImageCarte::GenerateImage(string NomImage, int height, int width, vector<vector<uint8_t> > &matrix) {
	ofstream img(NomImage + ".ppm");
	img << "P3" << endl;
	img << width << " " << height << endl;
	img << "255" << endl;
	GenerateGrid(img, height, width, matrix);
}