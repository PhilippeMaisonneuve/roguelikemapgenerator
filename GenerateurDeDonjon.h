#pragma once
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <numeric>
#include <random>
#include <utility>
#include <time.h>
#include <tuple>

using namespace std;

class GenerateurDeDonjon
{
public:
	enum roomType { Type1 = 2, Type2 = 3, Type3 = 4};
	const int VARIANCE_HAUTEUR_PIECE = 10;
	const int VARIANCE_LARGEUR_PIECE = 10;
	const int HAUTEUR_MINIMUM_PIECE = 4;
	const int LARGEUR_MINIMUM_PIECE = 4;

	GenerateurDeDonjon(int nbPiece, int width, int height);
	~GenerateurDeDonjon();

	int getNbPiece();
	vector<vector<uint8_t>> getCarte();
	void setNbPiece(int nbPiece);
	
private:
	bool construirePiece(roomType type = Type1);
	bool construirePiece(int hauteur, int largeur, int positionX, int positionY, int type = Type1);
	bool VerifieEspace(int largeur, int hauteur, int positionX, int positionY);
	int remplirPiece(int nbPiece);
	int _nbPiece;
	int _height;
	int _width; 

	vector<vector<uint8_t>> carte;
};
