#include "GenerateurDeDonjon.h"



GenerateurDeDonjon::GenerateurDeDonjon(int nbPiece, int width, int height) : _nbPiece(nbPiece),_width(width), _height(height)
{
	srand(time(NULL));
	carte.resize(width, std::vector<uint8_t>(height, 0));
	remplirPiece(nbPiece);
}


GenerateurDeDonjon::~GenerateurDeDonjon()
{
}

int GenerateurDeDonjon::getNbPiece()
{
	return _nbPiece;
}

vector<vector<uint8_t>> GenerateurDeDonjon::getCarte()
{
	return carte;
}

void GenerateurDeDonjon::setNbPiece(int nbPiece)
{
	 _nbPiece = nbPiece;
}
bool GenerateurDeDonjon::VerifieEspace(int largeur, int hauteur, int positionX, int positionY) {

	for (int i = -1; i < largeur + 1; i++) {
		for (int j = -1; j < hauteur + 1; j++)
		{

			if (positionX + largeur + i < _width && positionY + hauteur +j < _height)
			{

				if (carte[positionX + largeur +i][positionY + hauteur +j] != 0) {
					return false;
				}
			}
			else
			{
					return false;
			}
		}
	}
	return true;
}


bool GenerateurDeDonjon::construirePiece(roomType type)
{
	int positionX = rand() % _width ;
	int positionY = rand() % _height;
	int hauteurPiece = (rand() % VARIANCE_HAUTEUR_PIECE + HAUTEUR_MINIMUM_PIECE) *2 +1;
	int largeurPiece = (rand() % VARIANCE_LARGEUR_PIECE + LARGEUR_MINIMUM_PIECE)*2+1;
	if (VerifieEspace(largeurPiece, hauteurPiece, positionX, positionY)) 
	{
		for (int i = 0; i < largeurPiece; i++) {
			for (int j = 0; j < hauteurPiece; j++)
			{
				carte[positionX + largeurPiece+i][positionY + hauteurPiece+j] = 2;
			}
		}
	}
	else
		return false;
	return true;
}

int GenerateurDeDonjon::remplirPiece(int nbPiece)
{
	int PieceConstruite = 0;
	for  (int i = 0; i < nbPiece; i++)
	{
		construirePiece();
	}
	return 0;
}
