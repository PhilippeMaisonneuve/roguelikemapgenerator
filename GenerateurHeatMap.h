#pragma once
#include<fstream>
#include<vector>
#include<string>

using namespace std;

class GenerateurHeatMap 
{
public:
	GenerateurHeatMap();
	~GenerateurHeatMap();

	void GenerateImage(string NomImage, int height, int width, vector<vector<uint8_t> > &matrix);
	void GenerateImage() {};
	void GenerateGrid() {};

	void GenerateGrid(ofstream& img, int height, int width, vector<vector<uint8_t> > &matrix);

	void push_gradiant(ofstream& img, int grad);
};

