
int matrix_equals(const int* inmatdata1, const int* inmatdata2, int matorder);
void matrix_transpose(const int* inmatdata, int* outmatdata, int matorder);
void matrix_diagonal(const int* inmatdata, int* outmatdata, int matorder);
void matrix_multiply(const int* inmatdata1, const int* inmatdata2, int* outmatdata, int matorder);

void matrix_row_aver(const int* inmatdata, int* outmatdata, int matorder);

int* make_rotation(int matorder);

void matrix_rotate90(const int* inmatdata, int* outmatdata, int matorder);