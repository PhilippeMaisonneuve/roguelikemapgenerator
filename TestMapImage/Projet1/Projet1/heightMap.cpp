#include "heightMap.h"
#include "iostream"

using namespace std;

vector<vector<uint8_t>> heightMap::getCorruptionMap(){
	return corruptionMap;
}

heightMap::heightMap(int width, int height) : _width(width), _height(height)
{

		int Scale = 4;
		corruptionMap = generator(width, height, Scale);
}

heightMap::~heightMap()
{
}

void heightMap::addPerlinNoise(int height, int width, float scale, vector<vector<uint8_t>>& matrix) {

	PerlinNoise pn;
	int z = rand() / (rand() + 1);
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{

			float xScale = double(x*scale) / width;
			float yScale = double(y* scale) / height;
			int noise = 255 * pn.noise(xScale, yScale, z);
			matrix[x][y] = noise;

		}
	}
}

vector<vector<uint8_t>>  heightMap::generator(int height, int width, float scale) {
	srand(time(NULL));
	corruptionMap.resize(width, std::vector<uint8_t>(height, 0));
	if (scale <= 0) {
		scale = 1;
	}
	addPerlinNoise(height, width, scale, corruptionMap );


	
	return corruptionMap;
}

