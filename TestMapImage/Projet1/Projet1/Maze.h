#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <numeric>
#include <random>
#include <utility>
#include <time.h>
#include <tuple>
#include <stack>
#include "point.h"
#include "heightMap.h"

using namespace std;

class Maze
{
public:
	enum Direction { Haut, Droite, Bas, Gauche };
	enum Rarity { commun, rare, superRare, ultraRare  };
	enum roomType { turn, corridor, TCorner, deadEnd, Cross  };
	
	Maze(int height,  int width, int loop, int nbRoom);


	vector<vector<uint8_t>> getCarte();

	void drawMaze();


private:
	int tileSize = 15;
		
	int _height;
	int _width;
	int nbPointVisite;

	stack<point> pile;

	vector<int*> communRoom[2][5];
	vector<int*> rareRoom[2][5];
	vector<int*> superRoom[2][5];
	vector<int*> ultraRoom[2][5];

	vector<vector<point>> carteDeGeneration;
	vector<vector<uint8_t>> carte;

	vector<vector<uint8_t>> convertToCorrupt(int width, int height, vector<vector<uint8_t>>& matrix);
	bool adjacentLibre(int coordonneeX, int coordonneeY);
	pair<int, int> choisirAdjacent(int coordonneeX, int coordonneeY);
	void fillMaze(int coordonneeX, int coordonneY);
	bool makeLoop();
	void formMaze(int height, int width, int nbRoom);
	int compterOuverture(point pt);
	int* chooseRoom(roomType type, corruption corrupt);
	void pushPatern();
	void printCross(int x, int y);
	void printTCorner(int x, int y);
	void printDeadEnd(int x, int y);
	void printCorridor(int x, int y);
	void printTurn(int x, int y);
};

