#pragma once
#include "PerlinNoise.h"
#include "time.h"
#include <vector>

using namespace std;
class heightMap
{
public:
	heightMap(int width, int height);
	~heightMap();


	
	vector<vector<uint8_t>> getCorruptionMap();

private:
	int _width;
	int _height;
	vector<vector<uint8_t>> corruptionMap;
	vector<vector<uint8_t>> generator(int height, int width, float scale);
	void addPerlinNoise(int height, int width, float scale, vector<vector<uint8_t>>& matrix);
};

