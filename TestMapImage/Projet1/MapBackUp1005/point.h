#pragma once
struct point
{
public:
	point() :visited(true), openUp(false), openDown(false), openLeft(false), openRight(false), used(false) {};
	point(int x, int y) :visited(true), openUp(false), openDown(false), openLeft(false), openRight(false), coordonneX(x), coordonneY(y) {};
	~point();


	bool visited;
	bool openUp;
	bool openDown;
	bool openRight;
	bool openLeft;
	int coordonneX;
	int coordonneY;
	bool used;
};

