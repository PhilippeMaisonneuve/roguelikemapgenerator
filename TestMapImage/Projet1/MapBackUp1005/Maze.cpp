#include "Maze.h"
#include "matrixOperation.h"


vector<vector<uint8_t>> Maze::getCarte() {
	return carte;
};

void changerPoint(point& pointActuel) {

	uint8_t direction = rand() % 4;

	switch (direction)
	{
	case 0: 
		pointActuel.coordonneX++;
		pointActuel.openLeft = true;
		break;
	case 1:
		pointActuel.coordonneX--;
		pointActuel.openRight = true;
		break;
	case 2:
		pointActuel.coordonneY++;
		pointActuel.openDown = true;
		break;
	case 3:
		pointActuel.coordonneY--;
		pointActuel.openUp = true;
		break;
	default:
		break;
	}

}

bool Maze::adjacentLibre(int coordonneeX, int coordonneeY) {
	if (coordonneeX + 1 < _width){
		if (carteDeGeneration[coordonneeX + 1][coordonneeY].visited == false) {

			return true;
		}
	}
	if (coordonneeX - 1 >= 0){

		if (carteDeGeneration[coordonneeX - 1][coordonneeY].visited == false) {
	
			return true;
		}
	}
	if (coordonneeY + 1 < _height){

		if (carteDeGeneration[coordonneeX][coordonneeY + 1].visited == false) {

			return true;
		}
	}
	if (coordonneeY - 1 >= 0)
	{

		if (carteDeGeneration[coordonneeX][coordonneeY - 1].visited == false) {

			return true;
		}
	}
	

		return false;
}

pair<int, int> Maze::choisirAdjacent(int coordonneeX, int coordonneeY) {
	while (true) {
		int cote = rand() % 4;
		
		switch (cote)
		{
		case 0: 
			if (coordonneeX + 1 < _width){
			
				if (carteDeGeneration[coordonneeX + 1][coordonneeY].visited == false) {
			
					pair<int, int> retour = { coordonneeX + 1, coordonneeY };
					carteDeGeneration[coordonneeX][coordonneeY].openRight = true;
					carteDeGeneration[coordonneeX + 1][coordonneeY].openLeft = true;
					carteDeGeneration[coordonneeX + 1][coordonneeY].visited = true;
					fillMaze(retour.first, retour.second);
					return retour;
				}
		}
			break;
		case 1:
		
			if (coordonneeX - 1 >= 0){
			
				if (carteDeGeneration[coordonneeX - 1][coordonneeY].visited == false) {

					pair<int, int> retour = { coordonneeX - 1, coordonneeY };
					carteDeGeneration[coordonneeX][coordonneeY].openLeft = true;
					carteDeGeneration[coordonneeX - 1][coordonneeY].openRight = true;
					carteDeGeneration[coordonneeX - 1][coordonneeY].visited = true;
					fillMaze(retour.first, retour.second);
					return retour;
				}
			}
			break;
		case 2:
			if (coordonneeY + 1 < _height) {
				
				if (carteDeGeneration[coordonneeX][coordonneeY + 1].visited == false) {

					pair<int, int> retour = { coordonneeX , coordonneeY + 1 };
					carteDeGeneration[coordonneeX][coordonneeY].openDown = true;
					carteDeGeneration[coordonneeX][coordonneeY + 1].openUp = true;
					carteDeGeneration[coordonneeX][coordonneeY + 1].visited = true;
					fillMaze(retour.first, retour.second);
					return retour;
				}
			}
			break;

		case 3:
			if (coordonneeY - 1 >= 0) {
				
				if (carteDeGeneration[coordonneeX][coordonneeY - 1].visited == false) {

					pair<int, int> retour = { coordonneeX, coordonneeY - 1 };
					carteDeGeneration[coordonneeX][coordonneeY].openUp = true;
					carteDeGeneration[coordonneeX][coordonneeY - 1].openDown = true;
					carteDeGeneration[coordonneeX][coordonneeY - 1].visited = true;
					fillMaze(retour.first, retour.second);
					return retour;
				}
			}
			break;
		default:
			break;
		}		
	}
}
bool Maze::makeLoop() {

	while (1) {
		int coordonneeX = rand() % _width;
		int coordonneeY = rand() % _width;
		while (carteDeGeneration[coordonneeX][coordonneeY].used == false) {
			coordonneeX = rand() % _width;
			coordonneeY = rand() % _width;
		}

		int cote = rand() % 4;

		switch (cote)
		{
		case 0:
			if (coordonneeX + 1 < _width) {
				if (carteDeGeneration[coordonneeX + 1][coordonneeY].used == true) {
					carteDeGeneration[coordonneeX][coordonneeY].openRight = true;
					carteDeGeneration[coordonneeX + 1][coordonneeY].openLeft = true;
					return 1;
				}
			}
			break;
		case 1:

			if (coordonneeX - 1 >= 0) {
				if (carteDeGeneration[coordonneeX-1][coordonneeY].used == true) {

					carteDeGeneration[coordonneeX][coordonneeY].openLeft = true;
					carteDeGeneration[coordonneeX - 1][coordonneeY].openRight = true;
					return 1;
				}
			}
			break;
		case 2:
			if (coordonneeY + 1 < _height) {
				if (carteDeGeneration[coordonneeX][coordonneeY+1].used == true) {

					carteDeGeneration[coordonneeX][coordonneeY].openDown = true;
					carteDeGeneration[coordonneeX][coordonneeY + 1].openUp = true;
					return 1;
				}
			}
			break;

		case 3:
			if (coordonneeY - 1 >= 0) {
				if (carteDeGeneration[coordonneeX][coordonneeY - 1].used == true) {
		
					carteDeGeneration[coordonneeX][coordonneeY].openUp = true;
					carteDeGeneration[coordonneeX][coordonneeY - 1].openDown = true;
					return 1;
				}
			}

			break;
		default:
			break;
		}
	}
}
void Maze:: fillMaze(int coordonneeX, int coordonneY) {

	while (adjacentLibre(coordonneeX, coordonneY))
	{
		choisirAdjacent(coordonneeX, coordonneY);
	}
}

void Maze::formMaze(int height, int width, int nbRoom) {

	for (int j = 0; j < 4; j++)
	{		
		pair<int, int> point = { width/2-1, height/2-1 };

		int size = nbRoom;

		for (int i = 0; i < size;)
		{
			Direction cas = Direction(rand() % 4);
			switch (cas)
			{
			case Haut: 
	
				if (point.second + 1 < height)
				point.second++;
				break;
			case Droite:
			
				if (point.first + 1 < width)
					point.first++;
				break;
			case Bas:
			
				if (point.second - 1 >= 0)
					point.second--;
				break;
			case Gauche:
		
				if (point.first - 1 >= 0)
					point.first--;
				break;
			default:
				break;
			}


			if (carteDeGeneration[point.first][point.second].visited == 1) {

				carteDeGeneration[point.first][point.second].visited = 0;
				carteDeGeneration[point.first][point.second].used = 1;
				i++;
			}
		}
	}

}
   

Maze::Maze(int height, int width, int loop, int nbRoom) : _height(height), _width(width)
{
	nbPointVisite = 0;
	srand(time(NULL));
	pushPatern();

	carte.resize(width*tileSize, std::vector<uint8_t>(height*tileSize, 1));

	carteDeGeneration.resize(width, std::vector<point>(height));

	formMaze(height, width, nbRoom/4);
	carteDeGeneration[width/2-1][height/2-1].visited = 1;
	fillMaze(width/2-1, height/2-1);
	for (int i = 0; i < loop; i++)
	{
		makeLoop();
	}


}

int Maze::compterOuverture(point pt) {
	int nbOpen = 0;
	if (pt.openDown == 1) {
		nbOpen++;
	}
	if (pt.openUp == 1) {
		nbOpen++;
	}
	if (pt.openLeft == 1) {
		nbOpen++;
	}
	if (pt.openRight == 1) {
		nbOpen++;
	}
	return nbOpen;
}


int* Maze::chooseRoom(roomType type) {
	int rarityDice = rand() % 100;

	if (rarityDice > 97 && ultraRoom[type].size() != 0)
	{
		return ultraRoom[type][rand() % ultraRoom[type].size()];
	}
	if (rarityDice > 90 && superRoom[type].size() != 0)
	{
		return superRoom[type][rand() % superRoom[type].size()];
	}

	if (rarityDice > 70 && rareRoom[type].size() != 0)
	{
		return rareRoom[type][rand() % rareRoom[type].size()];
	}
	return communRoom[type][rand() % communRoom[type].size()];
	
}

void Maze::printCross(int x, int y) {
	int* room = chooseRoom(Cross);
	int k = 0;
	for (int i = 0; i < tileSize; i++) {
		for (int j = 0; j < tileSize; j++) {
			carte[j + x * tileSize][i + y * tileSize] = uint8_t(room[k]);
			k++;
		}
	}
}

void Maze::printTCorner(int x, int y) {
	
	int* room = chooseRoom(TCorner);
	 
	if (carteDeGeneration[x][y].openRight == 0) {
		int* temp = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		room = temp;
	}
	else if (carteDeGeneration[x][y].openDown == 0){
		int* temp = new int[tileSize*tileSize];
		int* temp2 = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		matrix_rotate90(temp, temp2, tileSize);
		delete[] temp;
		room = temp2;
	}
	else if (carteDeGeneration[x][y].openLeft == 0){
		int* temp = new int[tileSize*tileSize];
		int* temp2 = new int[tileSize*tileSize];
		int* temp3 = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		matrix_rotate90(temp, temp2, tileSize);
		matrix_rotate90(temp2, temp3, tileSize);
		delete[] temp;
		delete[] temp2;
		room = temp3;
	
	}
	int k = 0;

	for (int i = 0; i < tileSize; i++) {
		for (int j = 0; j < tileSize; j++) {
			
			carte[j + x * tileSize][i + y * tileSize] = uint8_t(room[k]);
			k++;
		}
	}
}

void Maze::printDeadEnd(int x, int y) {
	
	int* room = chooseRoom(deadEnd);
	
	if (carteDeGeneration[x][y].openRight == 1){
		int* temp = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		room = temp;
	
	}
	else if (carteDeGeneration[x][y].openDown == 1){
		int* temp = new int[tileSize*tileSize];
		int* temp2 = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		matrix_rotate90(temp, temp2, tileSize);
		delete[] temp;
		room = temp2;
	}
	
	else if (carteDeGeneration[x][y].openLeft == 1)
	{
		int* temp = new int[tileSize*tileSize];
		int* temp2 = new int[tileSize*tileSize];
		int* temp3 = new int[tileSize*tileSize];

		matrix_rotate90(room, temp, tileSize);
		matrix_rotate90(temp, temp2, tileSize);
		matrix_rotate90(temp2, temp3, tileSize);
		delete[] temp;
		delete[] temp2;
		room = temp3;
	}

	int k = 0;

	for (int i = 0; i < tileSize; i++) {
		for (int j = 0; j < tileSize; j++) {
			carte[j + x * tileSize][i + y * tileSize] = uint8_t(room[k]);
			k++;
		}
	}
}



void Maze::printCorridor(int x, int y) {
	int* room = chooseRoom(corridor);

	if (carteDeGeneration[x][y].openUp && carteDeGeneration[x][y].openDown){
		int* temp = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		room = temp;
	}

	int k = 0;
	for (int i = 0; i < tileSize; i++) {
		for (int j = 0; j < tileSize; j++) {
			carte[i + x * tileSize][j + y * tileSize] = uint8_t(room[k]);
			k++;
		}
	}

}

void Maze::printTurn(int x, int y) {

	int* room = chooseRoom(turn);
	
	if (carteDeGeneration[x][y].openUp && carteDeGeneration[x][y].openRight){
		int* temp = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		room = temp;
	
	}
	if (carteDeGeneration[x][y].openRight && carteDeGeneration[x][y].openDown){
		int* temp = new int[tileSize*tileSize];
		int* temp2 = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		matrix_rotate90(temp, temp2, tileSize);
		delete[] temp;
		room = temp2;
	}
	if (carteDeGeneration[x][y].openDown && carteDeGeneration[x][y].openLeft){
		int* temp = new int[tileSize*tileSize];
		int* temp2 = new int[tileSize*tileSize];
		int* temp3 = new int[tileSize*tileSize];
		matrix_rotate90(room, temp, tileSize);
		matrix_rotate90(temp, temp2, tileSize);
		matrix_rotate90(temp2, temp3, tileSize);
		delete[] temp;
		delete[] temp2;
		room = temp3;
	}


		int k = 0;
		for (int i = 0; i < tileSize; i++) {
		for (int j = 0; j < tileSize; j++) {
			carte[j + x * tileSize][i + y * tileSize] = uint8_t(room[k]);
			k++;
		}
	}
}

void Maze::drawMaze()
{
	for (int i = 0; i < _width; i++) {
		for (int j = 0; j < _height; j++) {

		
			int nbOpen = compterOuverture(carteDeGeneration[i][j]);
			if (nbOpen == 4) {
				printCross(i, j);
			
			}
			if (nbOpen == 3) {
				printTCorner(i, j);
			
			}
			if (nbOpen == 1) {
				printDeadEnd(i, j);
			
			}
			if (nbOpen == 2) {
				if (carteDeGeneration[i][j].openUp && carteDeGeneration[i][j].openDown ||
				carteDeGeneration[i][j].openLeft && carteDeGeneration[i][j].openRight )
				printCorridor(i, j);
				else
				printTurn(i,j);
			}
		
		}

	}
}


