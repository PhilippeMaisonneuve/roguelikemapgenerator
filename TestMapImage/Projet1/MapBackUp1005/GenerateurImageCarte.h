#pragma once
#include<fstream>
#include<vector>



using namespace std;
class GenerateurImageCarte 
{
public:
	GenerateurImageCarte();
	~GenerateurImageCarte();

	void GenerateImage(string NomImage, int height, int width, vector<vector<uint8_t> > &matrix);

private:
	void push_black(ofstream& img);
	void push_white(ofstream& img);
	void push_blue(ofstream& img);
	void push_red(ofstream& img);
	void push_green(ofstream& img);
	void push_purple(ofstream& img);
	void push_pink(ofstream& img);
	void push_yellow(ofstream& img);
	void push_lightBlue(ofstream& img);
	void push_gray(ofstream& img);

	void GenerateGrid(ofstream& img, int height, int width, vector<vector<uint8_t> > &matrix);

};

