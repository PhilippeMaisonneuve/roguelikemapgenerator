#include "GenerateurHeatMap.h"
#include "iostream"





GenerateurHeatMap::GenerateurHeatMap()
{
}


GenerateurHeatMap::~GenerateurHeatMap()
{
}

void GenerateurHeatMap::GenerateGrid(ofstream& img, int height, int width, vector<vector<uint8_t> > &matrix) {
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			int value = matrix[x][y];
			push_gradiant(img, value);
		}
	}
}


void GenerateurHeatMap::push_gradiant(ofstream & img, int grad)
{
		img << grad << " " << 0 << " " << 255-grad << endl;
}

void GenerateurHeatMap::GenerateImage(string NomImage, int height, int width, vector<vector<uint8_t> > &matrix) {
	ofstream img(NomImage + ".ppm");
	img << "P3" << endl;
	img << width << " " << height << endl;
	img << "255" << endl;
	GenerateGrid(img, height, width, matrix);
}