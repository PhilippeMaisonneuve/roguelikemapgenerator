#include "matrixOperation.h"

int matrix_equals(const int* inmatdata1, const int* inmatdata2, int matorder) {
	/* Variables */
	int r, c; /* Row/column indices */
	/* Go through elements */
	for (r = 0; r < matorder; ++r) {
		for (c = 0; c < matorder; ++c) {
			if (inmatdata1[c + r * matorder] != inmatdata2[c + r * matorder])
				return 0;
		}
	}
	return 1;
}


/****************************************/
/****************************************/

void matrix_transpose(const int* inmatdata, int* outmatdata, int matorder) {
	/* Variables */
	int r, c; /* Row/column indices */
	/* Go through the matrix elements */
	for (r = 0; r < matorder; ++r) {
		for (c = 0; c < matorder; ++c) {
			outmatdata[c + r * matorder] = inmatdata[r + c * matorder];
		}
	}
}

/****************************************/
/****************************************/

void matrix_diagonal(const int* inmatdata, int* outmatdata, int matorder) {
	/* Variables */
	int r, c; /* Row/column indices */
	/* Go through the matrix elements */
	for (r = 0; r < matorder; ++r) {
		for (c = 0; c < matorder; ++c) {
			if (c == r) {
				outmatdata[c + r * matorder] = inmatdata[c + r * matorder];
			}
			else {
				outmatdata[c + r * matorder] = 0;
			}
		}
	}
}

/****************************************/
/****************************************/

void matrix_multiply(const int* inmatdata1, const int* inmatdata2, int* outmatdata, int matorder) {
	/* Variables */
	int r, c; /* Row/column indices */
	int i;    /* Index for element calculation */
	int elem; /* Buffer for element calculation */
	/* Perform row x column multiplication */
	for (r = 0; r < matorder; ++r) {
		for (c = 0; c < matorder; ++c) {
			elem = 0;
			for (i = 0; i < matorder; ++i)
				elem += inmatdata1[i + r * matorder] * inmatdata2[c + i * matorder];
			outmatdata[c + r * matorder] = elem;
		}
	}
}

/****************************************/
/****************************************/

void matrix_row_aver(const int* inmatdata, int* outmatdata, int matorder) {
	/* Variables */
	int r, c; /* Row/column indices */
	int elem; /* Buffer for element calculation */
	/* Perform row x column multiplication */
	for (r = 0; r < matorder; ++r) {
		elem = 0;
		for (c = 0; c < matorder; ++c) {
			elem += inmatdata[c + r * matorder];
		}
		outmatdata[r] = elem / matorder;
	}
}

/****************************************/
/****************************************/

//void matrix_print(const int* inmatdata, int matorder) {
//	/* Variables */
//	int r, c; /* Row/column indices */
//	int elem; /* Buffer for element calculation */
//	/* Perform row x column multiplication */
//	for (r = 0; r < matorder; ++r) {
//		for (c = 0; c < matorder; ++c) {
//			std::cout << inmatdata[r*matorder+c] << " ";
//		}
//		std::cout << std::endl;
//	}
//}


int* make_rotation(int matorder) {
	int* tempOutPut = new int[matorder*matorder]{ 0 };
	for (int r = 0; r < matorder; ++r) {
		tempOutPut[r*matorder + matorder - r - 1] = 1;
	}
	return tempOutPut;
}

void matrix_rotate90(const int* inmatdata, int* outmatdata, int matorder)
{
	int* tempOutPut = new int[matorder*matorder];

	matrix_transpose(inmatdata, tempOutPut, matorder);
	int* rotationMatrix = make_rotation(matorder);
	matrix_multiply(tempOutPut, rotationMatrix, outmatdata, matorder);
	delete[] tempOutPut;
}
