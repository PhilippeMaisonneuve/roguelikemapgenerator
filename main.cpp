#include <iostream>
#include "Maze.h"
#include "GenerateurImageCarte.h"

using namespace std;


int main() {

	Maze maze1(15, 15, 30, 120);	
	maze1.drawMaze();
	GenerateurImageCarte generateur;
	vector<vector<uint8_t>> carte = maze1.getCarte();
	generateur.GenerateImage("Maze", 225, 225, carte);
}